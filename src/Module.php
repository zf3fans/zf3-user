<?php
declare(strict_types=1);
namespace Zf3Lib\User;

use JetBrains\PhpStorm\ArrayShape;
use Zf3Lib\Lib\Db;
use Zf3Lib\Lib\Controller\Controller as AbstractActionController;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Laminas\Uri\Http as HttpUri;
use Laminas\Mvc\MvcEvent;
use Laminas\ServiceManager\ServiceManager;
use Zf3Lib\User\Controller\Plugin as UserPlugin;
use Zf3Lib\User\Service\UserService;
use Zf3Lib\User\View\Helper\CurrentUser;
use Laminas\Session\{
    SessionManager,
    Container as SessionContainer,
    Exception\RuntimeException as SessionRuntimeException,
};
use Zf3Lib\Lib\Helper\Url;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
    
    #[ArrayShape(['factories' => "\Closure[]"])]
    public function getServiceConfig(): array
    {
        return [
            'factories' => [
                Service\UserService::class =>
                    fn (ServiceManager $sm) =>
                        new Service\UserService(
                            $sm->get(DbGateway\User\Users::class),
                            $sm->get(DbGateway\User\UserGroups::class),
                            $sm->get(Service\AuthorizationService::class),
                        ),

                Service\AuthorizationService::class =>
                    fn (ServiceManager $sm) =>
                        new Service\AuthorizationService(
                            $sm->get(DbGateway\User\Users::class),
                            $sm->get(Service\AccessTokenService::class),
                            $sm->get('Request'),
                        ),

                Service\RegistrationService::class =>
                    fn (ServiceManager $sm) =>
                        new Service\RegistrationService(
                            $sm->get(Service\UserService::class),
                        ),

                Service\RestorePasswordService::class =>
                    fn (ServiceManager $sm) =>
                        new Service\RestorePasswordService(
                            $sm->get(Service\UserService::class),
                        ),

                Service\AccessTokenService::class =>
                    fn (ServiceManager $sm) =>
                        new Service\AccessTokenService(
                            $sm->get(DbGateway\User\UserAccessTokens::class)
                        ),

                DbGateway\User\Users::class =>
                    fn (ServiceManager $sm) =>
                        new DbGateway\User\Users(
                            $sm->get(Db\AdapterManager::class),
                        ),

                DbGateway\User\UserGroups::class =>
                    fn (ServiceManager $sm) =>
                        new DbGateway\User\UserGroups(
                            $sm->get(Db\AdapterManager::class),
                        ),

                DbGateway\User\UserAccessTokens::class =>
                    fn (ServiceManager $sm) =>
                        new DbGateway\User\UserAccessTokens(
                            $sm->get(Db\AdapterManager::class),
                        ),

                DbGateway\User\UserApiRequests::class =>
                    fn (ServiceManager $sm) =>
                        new DbGateway\User\UserApiRequests(
                            $sm->get(Db\AdapterManager::class),
                        ),

                DbGateway\User\UserTariffs::class =>
                    fn (ServiceManager $sm) =>
                        new DbGateway\User\UserTariffs(
                            $sm->get(Db\AdapterManager::class),
                        ),
            ],
        ];
    }

    #[ArrayShape(['factories' => "\Closure[]"])]
    public function getControllerPluginConfig(): array
    {
        return [
            'factories' => [
                'currentUser' =>
                    fn (ServiceManager $sm) =>
                        new UserPlugin\CurrentUserPlugin(
                            $sm->get(UserService::class),
                        ),
            ],
        ];
    }

    #[ArrayShape(['factories' => "\Closure[]", 'aliases' => "string[]"])]
    public function getViewHelperConfig(): array
    {
        return [
            'factories' => [
                CurrentUser::class =>
                    fn (ServiceManager $sm) =>
                        new CurrentUser(
                            $sm->get(UserService::class),
                        ),
            ],
            'aliases' => [
                'currentUser' => CurrentUser::class,
            ],
        ];
    }

    public function onBootstrap(MvcEvent $event): void
    {
        $eventManager = $event->getApplication()->getEventManager();
        $eventManager->attach(MvcEvent::EVENT_BOOTSTRAP, [$this, 'initSession'], 1);
    }

    public function initSession(MvcEvent $event): void
    {
        try {
            /** @var SessionManager $sessionManager */
            $sessionManager = $event->getApplication()
                ->getServiceManager()
                ->get(SessionManager::class);
        } catch (NotFoundExceptionInterface|ContainerExceptionInterface) {
            return;
        }

        if ($sessionManager->sessionExists()) {
            return;
        }

        try {
            $sessionManager->start();
            SessionContainer::setDefaultManager($sessionManager);
        } catch (SessionRuntimeException) {
            $sessionManager->destroy();
        }
    }
}
