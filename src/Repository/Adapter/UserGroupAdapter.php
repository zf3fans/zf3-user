<?php
declare(strict_types=1);
namespace Zf3Lib\User\Repository\Adapter;

use Zf3Lib\Lib\Entity\EntityInterface;
use Zf3Lib\Lib\Repository\EntityAdapterInterface;
use Zf3Lib\User\Entity\User;
use Zf3Lib\User\Entity\UserGroup;
use Zf3Lib\User\Entity\UserInterface;
use Zf3Lib\Lib\Helper;

class UserGroupAdapter implements EntityAdapterInterface
{
    /**
     * @return UserGroup
     */
    protected function _getEmptyEntity(): EntityInterface
    {
        return new UserGroup();
    }

    /**
     * @param array $userData
     * @return UserGroup
     */
    protected function _getFromArray(array $userGroupData): EntityInterface
    {
        $userGroup = $this->_getEmptyEntity();

        $id         = (int) ($userGroupData['group_id'] ?? 0);
        $slug       = (string) ($userGroupData['slug'] ?? '');
        $name       = (string) ($userGroupData['name'] ?? '');
        $createdAt  = Helper\DateTime::getDtiOrNull($userGroupData['created_at'] ?? Helper\DateTime::DT_EMPTY);
        $updatedAt  = Helper\DateTime::getDtiOrNull($userGroupData['updated_at'] ?? Helper\DateTime::DT_EMPTY);


        $userGroup->setId($id);
        $userGroup->setSlug($slug);
        $userGroup->setName($name);
        $userGroup->setCreatedAt($createdAt);
        $userGroup->setUpdatedAt($updatedAt);

        return $userGroup;
    }

    /**
     * @return UserGroup
     */
    public function getEmpty(): EntityInterface
    {
        return $this->_getFromArray([]);
    }

    /**
     * @param array $entityData
     * @return UserGroup
     */
    public function fromArray(array $entityData): EntityInterface
    {
        return $this->_getFromArray($entityData);
    }

    /**
     * @param UserGroup $userGroup
     * @return array
     */
    public function toArray(EntityInterface $userGroup): array
    {
        throw new \RuntimeException('TODO: implement!!');
    }
}