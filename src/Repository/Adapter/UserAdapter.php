<?php
declare(strict_types=1);
namespace Zf3Lib\User\Repository\Adapter;

use Zf3Lib\Lib\Entity\EntityInterface;
use Zf3Lib\Lib\Repository\EntityAdapterInterface;
use Zf3Lib\User\DbGateway\User\UserGroups;
use Zf3Lib\User\Entity\Authorization;
use Zf3Lib\User\Entity\User;
use Zf3Lib\Lib\Helper;
use Zf3Lib\User\Entity\UserInterface;

class UserAdapter implements EntityAdapterInterface
{
    /**
     * @return User
     */
    protected function _getEmptyEntity(): UserInterface|EntityInterface
    {
        return new User();
    }

    /**
     * @param array $userData
     * @return User
     */
    protected function _getFromArray(array $userData): UserInterface|EntityInterface
    {
        $user = $this->_getEmptyEntity();


        $id             = (int) ($userData['user_id'] ?? 0);
        $login          = (string) ($userData['login'] ?? '');
        $passwordHash   = (string) ($userData['password'] ?? '');

        $isBlocked      = (bool) ($userData['is_blocked'] ?? false);
        $blockingReason = $userData['blocking_reason'] ?? '';
        $blockedAt      = Helper\DateTime::getDtiOrNull($userData['blocked_at'] ?? Helper\DateTime::DT_EMPTY);

        $createdAt      = Helper\DateTime::getDtiOrNull($userData['created_at'] ?? Helper\DateTime::DT_EMPTY);
        $updatedAt      = Helper\DateTime::getDtiOrNull($userData['updated_at'] ?? Helper\DateTime::DT_EMPTY);


        return $user
            ->setId($id)
            ->setLogin($login)
            ->setPasswordHash($passwordHash)
            ->setIsBlocked($isBlocked, $blockingReason)
            ->setBlockedAt($blockedAt)
            ->setAuthorization(new Authorization())
            ->setCreatedAt($createdAt)
            ->setUpdatedAt($updatedAt);
    }

    /**
     * @return User
     */
    public function getEmpty(): EntityInterface
    {
        return $this->_getFromArray([]);
    }

    /**
     * @param array $entityData
     * @return User
     */
    public function fromArray(array $entityData): EntityInterface
    {
        return $this->_getFromArray($entityData);
    }

    /**
     * @param User $entity
     * @return array
     */
    public function toArray(EntityInterface $entity): array
    {
        return [
            'user_id' => $entity->id(),
            'login' => $entity->login(),
            'password' => $entity->passwordHash(),
            'is_blocked' => $entity->isBlocked() ? 1 : 0,
            'blocking_reason' => $entity->blockingReason(),
            'blocked_at' => $entity->blockedAt() !== null
                ? $entity->createdAt()->format(Helper\DateTime::DT_FORMAT_FULL)
                : null,
            'created_at' => $entity->createdAt()->format(Helper\DateTime::DT_FORMAT_FULL),
            'updated_at' => $entity->updatedAt()->format(Helper\DateTime::DT_FORMAT_FULL),
        ];
    }
}