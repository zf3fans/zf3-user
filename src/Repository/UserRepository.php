<?php
declare(strict_types=1);
namespace Zf3Lib\User\Repository;

use Zf3Lib\Lib\Db\DbGatewayInterface;
use Zf3Lib\Lib\Db\SearchParamsInterface;
use Zf3Lib\Lib\Entity\EntityInterface;
use Zf3Lib\Lib\Helper\Arr;
use Zf3Lib\Lib\Repository\AbstractRepository;
use Zf3Lib\Lib\Repository\EntityAdapterInterface;
use Zf3Lib\User\DbGateway\User\UserGroups;
use Zf3Lib\User\DbGateway\User\Users;
use Zf3Lib\User\Entity\UserInterface;
use Zf3Lib\User\Repository\SearchParams\UserSearchParams;
use Laminas\Db\Sql;

class UserRepository extends AbstractRepository
{
    protected Users $userDbGateway;
    protected UserGroups $userGroupDbGateway;
    protected EntityAdapterInterface $userAdapter;
    protected EntityAdapterInterface $userGroupAdapter;

    public function __construct(
        Users $userDbGateway,
        UserGroups $userGroupDbGateway,
        EntityAdapterInterface $userAdapter,
        EntityAdapterInterface $userGroupAdapter,
    )
    {
        $this->userDbGateway = $userDbGateway;
        $this->userGroupDbGateway = $userGroupDbGateway;
        $this->userAdapter = $userAdapter;
        $this->userGroupAdapter = $userGroupAdapter;
    }

    protected function dbGateway(): DbGatewayInterface
    {
        return $this->userDbGateway();
    }

    protected function userDbGateway(): DbGatewayInterface
    {
        return $this->userDbGateway;
    }

    protected function userGroupDbGateway(): DbGatewayInterface
    {
        return $this->userGroupDbGateway;
    }

    public function hydrator(): EntityAdapterInterface
    {
        return $this->userAdapter();
    }

    protected function userAdapter(): EntityAdapterInterface
    {
        return $this->userAdapter;
    }

    protected function userGroupAdapter(): EntityAdapterInterface
    {
        return $this->userGroupAdapter;
    }


    // region Get

    /**
     * @return UserInterface
     */
    public function getEmpty(): EntityInterface
    {
        /** @var UserInterface $userEmpty */
        $userEmpty = parent::getEmpty();

        return $userEmpty;
    }

    /**
     * @param int $entityId
     * @return UserInterface
     */
    public function getById(int $entityId): EntityInterface
    {
        /** @var UserInterface $user */
        $user = parent::getById($entityId);

        return $user;
    }

    public function getByLogin(string $login): UserInterface
    {
        return Arr::first(
            $this->getList(
                (new UserSearchParams())->setLogins([$login])
            )
        ) ?? $this->getEmpty();
    }

    public function getList(SearchParamsInterface $searchParams): array
    {
        $users = parent::getList($searchParams);
        if (count($users) === 0) {
            return [];
        }

        if ($searchParams->needEnrich()) {
            $users = $this->enrich($users);
        }

        return $users;
    }

    // region Enrich

    /**
     * @param array<UserInterface> $users
     * @return array<UserInterface>
     */
    protected function enrich(array $users): array
    {
        $users = $this->enrichWithGroups($users);

        return $users;
    }

    protected function enrichWithGroups(array $users): array
    {
        $userIds = $this->getIds($users);

        $rawUserGroupsGrouped = $this->userGroupDbGateway->getUsersGroups($userIds);
        $userGroupAdapter = $this->userGroupAdapter();

        /** @var UserInterface $user */
        foreach ($users as $userId => $user) {
            $rawUserGroups = $rawUserGroupsGrouped[$userId] ?? [];
            $userGroups = [];
            foreach ($rawUserGroups as $rawUserGroup) {
                $userGroup = $userGroupAdapter->fromArray($rawUserGroup);
                $userGroups[$userGroup->id()] = $userGroup;
            }
            $user->setGroups($userGroups);

            $users[$user->id()] = $user;
        }

        return $users;
    }

    // endregion Enrich

    // endregion Get


    // region Search Params

    /**
     * @param UserSearchParams|SearchParamsInterface $searchParams
     * @return Sql\Where
     */
    protected function getWhere(SearchParamsInterface $searchParams): Sql\Where
    {
        $where = parent::getWhere($searchParams);
        if (!($searchParams instanceof UserSearchParams)) {
            return $where;
        }

        if ($searchParams->logins() !== null) {
            $where->addPredicate(
                new Sql\Predicate\In(
                    'login',
                    $searchParams->logins(),
                )
            );
        }

        return $where;
    }

    // endregion Search Params
}