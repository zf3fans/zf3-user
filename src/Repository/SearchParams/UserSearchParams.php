<?php
declare(strict_types=1);
namespace Zf3Lib\User\Repository\SearchParams;

use Zf3Lib\Lib\Db\SearchParams;
use Zf3Lib\Lib\Db\SearchParamsInterface;

class UserSearchParams extends SearchParams
{
    // region Params

    protected ?array $logins = null;

    public function logins(): ?array
    {
        return $this->logins;
    }

    public function setLogins(?array $logins): static
    {
        $this->logins = $logins;
        return $this;
    }

    // endregion Params
}