<?php
declare(strict_types=1);
namespace Zf3Lib\User\Entity\Adapter;

use Zf3Lib\User\Entity\UserInterface;

interface UserAdapterInterface
{
    public function getEmpty(): UserInterface;
    public function fromDb(array $userData): UserInterface;
    public function toDb(UserInterface $user): array;
}