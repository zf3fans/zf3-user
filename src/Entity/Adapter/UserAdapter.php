<?php
declare(strict_types=1);
namespace Zf3Lib\User\Entity\Adapter;

use Zf3Lib\User\Entity\User;
use Zf3Lib\User\Entity\UserInterface;
use Zf3Lib\Lib\Helper;

class UserAdapter implements UserAdapterInterface
{
    public function getEmpty(): UserInterface
    {
        return new User();
    }

    public function fromDb(array $userData): UserInterface
    {
        $user = $this->getEmpty();


        $id             = (int) ($userData['user_id'] ?? 0);
        $login          = (string) ($userData['login'] ?? '');
        $passwordHash   = (string) ($userData['password_hash'] ?? '');

        $isBlocked      = (bool) ($userData['is_blocked'] ?? false);
        $blockingReason = $userData['blocking_reason'] ?? '';
        $blockedAt      = Helper\DateTime::getDtiOrNull($tokenData['blocked_at'] ?? Helper\DateTime::DT_EMPTY);

        $createdAt      = Helper\DateTime::getDtiOrNull($tokenData['created_at'] ?? Helper\DateTime::DT_EMPTY);
        $updatedAt      = Helper\DateTime::getDtiOrNull($tokenData['updated_at'] ?? Helper\DateTime::DT_EMPTY);


        $user->setId($id);
        $user->setLogin($login);
        $user->setPasswordHash($passwordHash);
        $user->setIsBlocked($isBlocked, $blockingReason);
        $user->setBlockedAt($blockedAt);
        $user->setCreatedAt($createdAt);
        $user->setUpdatedAt($updatedAt);

        return $user;
    }

    public function toDb(UserInterface $user): array
    {
        throw new \RuntimeException('TODO: implement!!');
    }
}