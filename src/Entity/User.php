<?php
declare(strict_types=1);
namespace Zf3Lib\User\Entity;

use DateTimeImmutable;
use Zf3Lib\Lib\Helper;
use JetBrains\PhpStorm\Pure;

class User
{
    // region Constructor

    public function __construct(?array $userData = null)
    {
        if ($userData === null) {
            $this->initDefault();
        } else {
            $this->setData($userData);
        }
    }

    // endregion Constructor


    // region Properties

    private int $id;

    public function id(): int
    {
        return $this->id;
    }


    private string $login;

    public function login(): string
    {
        return $this->login;
    }


    private string $passwordHash;

    public function passwordHash(): string
    {
        return $this->passwordHash;
    }


    private bool $isBlocked;

    public function isBlocked(): bool
    {
        return $this->isBlocked;
    }


    private string $blockingReason;

    public function blockingReason(): string
    {
        return $this->blockingReason;
    }


    private ?DateTimeImmutable $createdAt;

    public function createdAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }


    private ?DateTimeImmutable $updatedAt;

    public function updatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }


    private ?DateTimeImmutable $blockedAt;

    public function blockedAt(): ?DateTimeImmutable
    {
        return $this->blockedAt;
    }


    private array $userData;

    public function data(): array
    {
        return $this->userData;
    }


    private Authorization $authorization;

    public function authorization(): Authorization
    {
        return $this->authorization;
    }

    public function setAuthorization(Authorization $authorization): void
    {
        $this->authorization = $authorization;
    }

    /**
     * @var UserGroup[]
     */
    private array $groups;

    public function groups(): array
    {
        return $this->groups;
    }

    public function setGroups(array $userGroups = []): void
    {
        $this->groups = $userGroups;
    }

    // endregion Properties


    // region Set Data

    public function initDefault(): void
    {
        $this->userData         = [];

        $this->id               = 0;
        $this->login            = '';
        $this->passwordHash     = '';

        $this->isBlocked        = false;
        $this->blockingReason   = '';

        $this->updatedAt        = null;
        $this->createdAt        = null;
        $this->blockedAt        = null;

        $this->groups           = [];
        $this->authorization    = new Authorization();
    }

    public function setData(array $userData = []): void
    {
        $this->userData = $userData;

        $this->id               = (int) ($userData['user_id'] ?? 0);
        $this->login            = $userData['login'] ?? '';
        $this->isBlocked        = (bool) ($userData['is_blocked'] ?? false);
        $this->blockingReason   = $userData['blocking_reason'] ?? '';

        $this->createdAt  = Helper\DateTime::getDtiOrNull($tokenData['created_at'] ?? Helper\DateTime::DT_EMPTY);
        $this->updatedAt  = Helper\DateTime::getDtiOrNull($tokenData['updated_at'] ?? Helper\DateTime::DT_EMPTY);
        $this->blockedAt  = Helper\DateTime::getDtiOrNull($tokenData['blocked_at'] ?? Helper\DateTime::DT_EMPTY);
    }

    // endregion Set Data


    // region Groups & Acl

    #[Pure]
    public function hasGroup(string $groupSLug): bool
    {
        foreach ($this->groups as $group) {
            if ($group->slug() === $groupSLug) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return bool
     * @deprecated Изначально deprecated. Как будет первая возможность, сразу переделать на ACL
     */
    #[Pure]
    public function isAdmin(): bool
    {
        return $this->hasGroup(Acl::GROUP_ADMIN);
    }

    // endregion Custom Methods
}