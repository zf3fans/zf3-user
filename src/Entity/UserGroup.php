<?php
declare(strict_types=1);
namespace Zf3Lib\User\Entity;

use DateTimeImmutable;
use Zf3Lib\Lib\Helper;

class UserGroup
{
    // region Properties

    private int $id;

    public function id(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }


    private string $slug;

    public function slug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }


    private string $name;

    public function name(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }


    private ?DateTimeImmutable $createdAt;

    public function createdAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeImmutable $createdAt): void
    {
        $this->createdAt = $createdAt;
    }


    private ?DateTimeImmutable $updatedAt;

    public function updatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeImmutable $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    // endregion Properties


    // region Constructor

    public function __construct(?array $userGroupData)
    {
        $this->id         = (int) ($userGroupData['group_id'] ?? 0);
        $this->slug       = (string) ($userGroupData['slug'] ?? '');
        $this->name       = (string) ($userGroupData['name'] ?? '');

        $this->createdAt  = Helper\DateTime::getDtiOrNull($userGroupData['created_at'] ?? Helper\DateTime::DT_EMPTY);
        $this->updatedAt  = Helper\DateTime::getDtiOrNull($userGroupData['updated_at'] ?? Helper\DateTime::DT_EMPTY);
    }

    // endregion Constructor
}