<?php
declare(strict_types=1);
namespace Zf3Lib\User\Entity;

class Acl
{
    public const GROUP_ADMIN           = 'admin'; // сразу deprecated
    public const GROUP_SYSTEM_API      = 'system-api';
}
