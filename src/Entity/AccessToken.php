<?php
declare(strict_types=1);
namespace Zf3Lib\User\Entity;

use DateTimeImmutable;
use Zf3Lib\Lib\Helper;

class AccessToken
{
    // region Properties

    private int $tokenId;

    public function id(): int
    {
        return $this->tokenId;
    }


    private string $hash;

    public function hash(): string
    {
        return $this->hash;
    }


    private string $salt;

    public function salt(): string
    {
        return $this->salt;
    }

    public const TYPE_API              = 'api';
    public const TYPE_AUTH             = 'auth';
    public const TYPE_PASSWORD_RECOVER = 'password_recover';

    public const TYPES = [
        self::TYPE_API,
        self::TYPE_AUTH,
        self::TYPE_PASSWORD_RECOVER,
    ];


    private string $type;

    public function type(): string
    {
        return $this->type;
    }


    private int $userId;

    public function userId(): int
    {
        return $this->userId;
    }

    private ?DateTimeImmutable $validUntil;

    public function validUntil(): ?DateTimeImmutable
    {
        return $this->validUntil;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        if (
            $this->validUntil === null ||
            ($this->validUntil->getTimestamp() > (new DateTimeImmutable('now'))->getTimestamp())
        ) {
            return true;
        }
        return false;
    }


    private ?DateTimeImmutable $createdAt;

    public function createdAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    // endregion Properties


    // region Constructor

    public function __construct(?array $tokenData)
    {
        $this->hash       = $tokenData['token_hash'] ?? '';
        $this->salt       = $tokenData['token_salt'] ?? '';
        $this->type       = $tokenData['token_type'] ?? '';
        $this->userId     = (int) ($tokenData['user_id'] ?? 0);
        $this->tokenId    = (int) ($tokenData['token_id'] ?? 0);

        $this->validUntil = Helper\DateTime::getDtiOrNull($tokenData['valid_until'] ?? Helper\DateTime::DT_EMPTY);
        $this->createdAt  = Helper\DateTime::getDtiOrNull($tokenData['created_at'] ?? Helper\DateTime::DT_EMPTY);
    }

    // endregion Constructor
}
