<?php
declare(strict_types=1);
namespace Zf3Lib\User\Entity;

class Authorization
{
    // region Construct

    public function __construct()
    {
        $this->login                    = '';
        $this->isAuthorized             = false;
        $this->authMethod               = self::AUTH_METHOD__UNKNOWN;
        $this->authorizationFailReason  = self::AUTH_FAIL_REASON__NONE;
        $this->accessToken              = new AccessToken([]);
    }

    // endregion Construct


    // region Login

    private string $login;

    public function login(): string
    {
        return $this->login;
    }

    public function setLogin(string $login): Authorization
    {
        $this->login = $login;
        return $this;
    }

    // endregion Login


    // region Is Authorized

    private bool $isAuthorized;

    public function isAuthorized(): bool
    {
        return $this->isAuthorized;
    }

    public function setIsAuthorized(bool $isAuthorized): Authorization
    {
        $this->isAuthorized = $isAuthorized;
        return $this;
    }

    // endregion Is Authorized


    // region Authorization Method

    public const AUTH_METHOD__UNKNOWN    = '';
    public const AUTH_METHOD__BY_SESSION = 'session';
    public const AUTH_METHOD__BY_QUERY   = 'query';
    public const AUTH_METHOD__BY_TOKEN   = 'token';

    public const AUTH_METHODS = [
        self::AUTH_METHOD__UNKNOWN,
        self::AUTH_METHOD__BY_SESSION,
        self::AUTH_METHOD__BY_QUERY,
        self::AUTH_METHOD__BY_TOKEN,
    ];


    private string $authMethod;

    public function authMethod(): string
    {
        return $this->authMethod;
    }

    public function setAuthMethod(string $authMethod): Authorization
    {
        if (in_array($authMethod, self::AUTH_METHODS, true)) {
            $this->authMethod = $authMethod;
        }

        return $this;
    }

    // endregion Authorization Method


    // region Authorization Fail Reason

    public const AUTH_FAIL_REASON__NONE                    = '';
    public const AUTH_FAIL_REASON__USER_NOT_LOGGED         = 'user_not_logged';
    public const AUTH_FAIL_REASON__EMPTY_LOGIN             = 'empty_login';
    public const AUTH_FAIL_REASON__EMPTY_PASSWORD          = 'empty_password';
    public const AUTH_FAIL_REASON__WRONG_PASSWORD          = 'wrong_password';
    public const AUTH_FAIL_REASON__USER_NOT_FOUND          = 'user_not_found';
    public const AUTH_FAIL_REASON__USER_BLOCKED            = 'user_blocked';
    public const AUTH_FAIL_REASON__EMPTY_TOKEN             = 'empty_token';        // не передали токен в запросе
    public const AUTH_FAIL_REASON__TOKEN_NOT_FOUND         = 'token_not_found';    // в базе нет запрошенного токена
    public const AUTH_FAIL_REASON__TOKEN_EXPIRED           = 'token_expired';      // токен просрочен
    public const AUTH_FAIL_REASON__ACCESS_NOT_GRANTED      = 'access_not_granted';      // пользователь может быть авторизован по токену, но у него нет доступа к запрашиваемому ресурсу

    public const AUTH_FAIL_REASONS = [
        self::AUTH_FAIL_REASON__NONE,
        self::AUTH_FAIL_REASON__USER_NOT_LOGGED,
        self::AUTH_FAIL_REASON__EMPTY_LOGIN,
        self::AUTH_FAIL_REASON__EMPTY_PASSWORD,
        self::AUTH_FAIL_REASON__WRONG_PASSWORD,
        self::AUTH_FAIL_REASON__USER_NOT_FOUND,
        self::AUTH_FAIL_REASON__USER_BLOCKED,
        self::AUTH_FAIL_REASON__EMPTY_TOKEN,
        self::AUTH_FAIL_REASON__TOKEN_NOT_FOUND,
        self::AUTH_FAIL_REASON__TOKEN_EXPIRED,
        self::AUTH_FAIL_REASON__ACCESS_NOT_GRANTED,
    ];

    private string $authorizationFailReason;

    public function authorizationFailReason(): string
    {
        return $this->authorizationFailReason;
    }

    public function setAuthorizationFailReason(string $authFailReason): Authorization
    {
        if (in_array($authFailReason, self::AUTH_FAIL_REASONS, true)) {
            $this->authorizationFailReason = $authFailReason;
        }

        return $this;
    }

    // endregion Authorization Fail Reason


    // region Access Token

    private AccessToken $accessToken;

    public function accessToken(): AccessToken
    {
        return $this->accessToken;
    }

    public function setAccessToken(AccessToken $accessToken): Authorization
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    // endregion Access Token
}