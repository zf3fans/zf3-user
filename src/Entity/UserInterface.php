<?php
declare(strict_types=1);
namespace Zf3Lib\User\Entity;

use DateTimeImmutable;

interface UserInterface
{
    public function id(): int;
    public function setId(int $id): static;

    public function login(): string;
    public function setLogin(string $passwordHash): static;

    public function passwordHash(): string;
    public function setPasswordHash(string $passwordHash): static;

    public function isBlocked(): bool;
    public function blockingReason(): string;
    public function setIsBlocked(bool $isBlocked, string $blockingReason = ''): static;

    public function blockedAt(): ?DateTimeImmutable;
    public function setBlockedAt(?DateTimeImmutable $blockedAt): static;

    public function createdAt(): DateTimeImmutable;
    public function setCreatedAt(DateTimeImmutable $createdAt): static;

    public function updatedAt(): DateTimeImmutable;
    public function setUpdatedAt(DateTimeImmutable $updatedAt): static;

    public function isAuthorized(): bool;
    public function authorization(): Authorization;
    public function setAuthorization(Authorization $authorization): static;

    public function groups(): array;
    public function setGroups(array $userGroups): static;

    public function isGuest(): bool;
    public function isAdmin(): bool;
}