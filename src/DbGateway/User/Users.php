<?php
declare(strict_types=1);
namespace Zf3Lib\User\DbGateway\User;

use JetBrains\PhpStorm\ArrayShape;
use Zf3Lib\Lib\Db\AbstractDbGateway as DbModel;

class Users extends DbModel
{
    public const TABLE = 'users';

    #[ArrayShape(['default' => "string[]"])]
    protected function scenarios(): array
    {
        return [
            'default' => [
                'user_id',
                'login',
                'password',
                'is_blocked',
                'blocking_reason',
                'blocked_at',
                'created_at',
                'updated_at',
            ],
        ];
    }

    private static $cache = [];

    public function getById(int $userId): ?array
    {
        if (isset(self::$cache['by_id'][$userId])) {
            return self::$cache['by_id'][$userId];
        }

        $userData = $this->findOne(['user_id' => $userId]);

        self::$cache['by_id'][$userId] = $userData;
        return $userData;
    }

    public function getByLogin(string $login): ?array
    {
        if (isset(self::$cache['by_login'][$login])) {
            return self::$cache['by_login'][$login];
        }

        $userData = $this->findOne(['login' => $login]);

        self::$cache['by_login'][$login] = $userData;
        return $userData;
    }

    public function hash(?string $password): string
    {
        return ($password !== null)
            ? md5($password)
            : '';
    }

    public function insert(array $record = []): int
    {
        $record['created_at'] = $record['updated_at'] = date('Y-m-d H:i:s');
        return parent::insert($record);
    }

    public function update($data, $where = null): int
    {
        $data['updated_at'] = date('Y-m-d H:i:s');
        return parent::update($data, $where);
    }

    protected function prepareFromDb(?array $record): ?array
    {
        if (empty($record)) {
            return null;
        }

        $record['user_id']    = (int) ($record['user_id'] ?? 0);
        $record['is_blocked'] = (bool) ($record['is_blocked'] ?? false);

        return $record;
    }
}