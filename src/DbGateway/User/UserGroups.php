<?php
declare(strict_types=1);
namespace Zf3Lib\User\DbGateway\User;

use JetBrains\PhpStorm\ArrayShape;
use Zf3Lib\Lib\Db\AbstractDbGateway as DbModel;
use Zf3Lib\Lib\Helper\Arr;
use Laminas\Db\Sql;

class UserGroups extends DbModel
{
    public const TABLE       = 'user_groups';
    public const TABLE_LINKS = 'user_group_links';

    #[ArrayShape(['default' => "string[]"])]
    protected function scenarios(): array
    {
        return [
            'default' => [
                'group_id',
                'slug',
                'name',
                'created_at',
                'updated_at',
            ],
        ];
    }

    public function insert(array $record = []): int
    {
        $record['created_at'] = $record['updated_at'] = date('Y-m-d H:i:s');
        return parent::insert($record);
    }

    public function update($data, $where = null): int
    {
        $data['updated_at'] = date('Y-m-d H:i:s');
        return parent::update($data, $where);
    }

    protected function prepareFromDb(?array $record): ?array
    {
        if ($record === null) {
            return null;
        }

        $record['group_id'] = (int) $record['group_id'];
        if (isset($record['user_id'])) {
            $record['user_id'] = (int) $record['user_id'];
        }
        return $record;
    }

    public function getUsersGroups(array $userIds): array
    {
        $userIds = Arr::filterArrayOfInt($userIds);
        if (count($userIds) === 0) return [];

        $sql = new Sql\Sql($this->adapter);
        $select = $sql->select()
            ->from(['ug' => static::TABLE])
            ->join(['ugl' => static::TABLE_LINKS], 'ug.group_id = ugl.group_id')
            ->where(new Sql\Where([
                new Sql\Predicate\In('ugl.user_id', $userIds)
            ]));

        $usersGroups = [];
        $result = $this->fetchAll($select);
        foreach ($result as $row) {
            $row = $this->prepareFromDb($row);
            if ($row === null) continue;

            $groupId = $row['group_id'];
            $userId = $row['user_id'];

            if (!isset($usersGroups[$userId])) {
                $usersGroups[$userId] = [];
            }

            $usersGroups[$userId][$groupId] = $row;
        }

        return $usersGroups;
    }

    public function getUserGroups(int $userId): array
    {
        $usersGroups = $this->getUsersGroups([$userId]);
        return $usersGroups[$userId] ?? [];
    }
}