<?php
declare(strict_types=1);
namespace Zf3Lib\User\DbGateway\User;

use JetBrains\PhpStorm\ArrayShape;
use Zf3Lib\Lib\Db\AbstractDbGateway as DbModel;

class UserApiRequests extends DbModel
{
    const TABLE = 'user_api_requests';

    #[ArrayShape(['default' => "string[]"])]
    protected function scenarios(): array
    {
        return [
            'default' => [
                'request_id',
                'request_action',           // Запрашиваемое действие
                'request_result',           // 'accept' | 'accept_reinit' | 'reject'
                'request_result_reason',    //
                'token_id',                 // ID токена, если пришедший токен найден
                'token_hash',               // пришедший хеш токена
                'user_id',                  // ID пользователя по токену
                'solution_id',              // ID программы, от которой пришёл запрос
                'solution_platform',
                'solution_category',
                'solution_version',         // Версия программы, от которой пришёл запрос
                'ip',                       // IP пользователя
                'requested_at',             // Дата/время пришедшего запроса
            ],
        ];
    }

    public function insert(array $record = []): int
    {
        if (!isset($record['requested_at'])) {
            $record['requested_at'] = date('Y-m-d H:i:s');
        }

        return parent::insert($record);
    }

    protected function prepareFromDb(?array $record): ?array
    {
        if ($record === null) {
            return null;
        }
        $record['request_id']       = (int) ($record['request_id'] ?? 0);
        $record['token_id']         = (int) ($record['token_id'] ?? 0);
        $record['user_id']          = (int) ($record['user_id'] ?? 0);

        return $record;
    }
}