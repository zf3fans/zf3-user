<?php
declare(strict_types=1);
namespace Zf3Lib\User\DbGateway\User;

use JetBrains\PhpStorm\ArrayShape;
use Zf3Lib\Lib\Db\AbstractDbGateway as DbModel;

class UserAccessTokens extends DbModel
{
    const TABLE = 'user_access_tokens';

    #[ArrayShape(['default' => "string[]"])]
    protected function scenarios(): array
    {
        return [
            'default' => [
                'token_id',
                'token_hash',
                'token_salt',
                'token_type',
                'user_id',
                'valid_until',
                'created_at',
            ],
        ];
    }

    public function insert(array $record = []): int
    {
        $record['created_at'] = date('Y-m-d H:i:s');
        return parent::insert($record);
    }

    protected function prepareFromDb(?array $record): ?array
    {
        if (empty($record)) {
            return null;
        }

        $record['token_id'] = (int) ($record['token_id'] ?? 0);
        $record['user_id']  = (int) ($record['user_id'] ?? 0);

        return $record;
    }
}