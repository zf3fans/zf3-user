<?php
declare(strict_types=1);
namespace Zf3Lib\User\DbGateway\User;

use JetBrains\PhpStorm\ArrayShape;
use Zf3Lib\Lib\Db\AbstractDbGateway as DbModel;

class UserTariffs extends DbModel
{
    const TABLE = 'user_tariffs';

    #[ArrayShape(['default' => "string[]"])]
    protected function scenarios(): array
    {
        return [
            'default' => [
                'tariff_id',
                'user_id',
                'code',
                'valid_until',
                'created_at',
            ],
        ];
    }

    public function insert(array $record = []): int
    {
        $record['created_at'] = date('Y-m-d H:i:s');
        return parent::insert($record);
    }
}