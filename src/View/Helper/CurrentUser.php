<?php
declare(strict_types=1);
namespace Zf3Lib\User\View\Helper;

use Zf3Lib\User\Entity\User;
use Zf3Lib\User\Service\UserService;
use Laminas\View\Helper\AbstractHelper;

class CurrentUser extends AbstractHelper
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function __invoke(): User
    {
        return $this->userService->getCurrentUser();
    }
}