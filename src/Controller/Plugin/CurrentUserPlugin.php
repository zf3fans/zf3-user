<?php
declare(strict_types=1);
namespace Zf3Lib\User\Controller\Plugin;

use Zf3Lib\User\Entity\User;
use Laminas\Mvc\Controller\Plugin\AbstractPlugin;
use Zf3Lib\User\Service\UserService;

class CurrentUserPlugin extends AbstractPlugin
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    private function getUserService(): UserService
    {
        return $this->userService;
    }

    public function __invoke(): User
    {
        return $this->getUserService()->getCurrentUser();
    }
}