<?php
declare(strict_types=1);
namespace Zf3Lib\User\Service;

use Laminas\Session\Container as SessionContainer;
use Zf3Lib\User\Entity\{
    User,
    UserGroup,
    Authorization,
};
use Zf3Lib\User\DbGateway\User\{
    Users,
    UserGroups
};
use Exception;

class UserService
{
    private Users $mUsers;
    private UserGroups $mUserGroups;
    private AuthorizationService $authorizationService;

    public function __construct(Users $mUsers, UserGroups $mUserGroups, AuthorizationService $authorizationService)
    {
        $this->mUsers               = $mUsers;
        $this->mUserGroups          = $mUserGroups;
        $this->authorizationService = $authorizationService;
    }


    private static ?int $currentUserId = null;
    private static array $users = [];
    public const BLOCK_REASON_REGISTRATION_NOT_FINISHED = 'registration_not_finished';


    // region Utils

    /**
     *
     * @param ?string $method
     * @return User
     */
    public function getCurrentUser(?string $method = null): User
    {
        if (self::$currentUserId !== null) {
            return self::$users[self::$currentUserId];
        }

        $authorization = $this->getAuthorization($method);

        $user = ($method === Authorization::AUTH_METHOD__BY_TOKEN)
            ? $this->getUserFromDbById($authorization->accessToken()->userId())
            : $this->getUserFromDbByLogin($authorization->login());
        $user->setAuthorization($authorization);

        if ($user->id() === 0) {
            return $user;
        }

        self::$currentUserId = $user->id();
        self::$users[$user->id()] = $user;

        return $user;
    }

    /**
     * Возвращает экземпляр класса Authorization
     * Если пользователь ещё не авторизован, пытается произвести авторизацию:
     * - либо указанным методом
     * - либо если метод не указан - всеми доступными методами
     *
     * @param ?string $method
     * @return Authorization
     */
    private function getAuthorization(?string $method = null): Authorization
    {
        return $this->authorizationService->get($method);
    }

    /**
     * Сохраняет авторизацию пользователя в сессию
     * @param User $user
     */
    public function storeAuth(User $user): void
    {
        $sessionContainer = new SessionContainer('auth');
        $sessionContainer->offsetSet('login', $user->login());
    }

    /**
     * Разлогинивает пользователя
     * @param User $user
     */
    public function logout(User $user): void
    {
        if (!$user->authorization()->isAuthorized()) {
            return;
        }

        $sessionContainer = new SessionContainer('auth');
        $sessionContainer->offsetUnset('login');

        if (isset(self::$users[$user->id()])) {
            unset(self::$users[$user->id()]);
        }
    }

    // endregion Utils


    // region DB Interaction

    /**
     * @param int $userId
     * @return User
     */
    public function getUserFromDbById(int $userId): User
    {
        $userData = $this->mUsers->getById($userId);
        return $this->getUser($userData);
    }

    /**
     * Возвращает экземпляр класса User по логину
     * @param string $login
     * @return User
     */
    public function getUserFromDbByLogin(string $login): User
    {
        $userData = $this->mUsers->getByLogin($login);
        return $this->getUser($userData);
    }

    public function getUserAnalyzator(): User
    {
        $userAnalyzatorLogin = getenv('USER_ANALYZATOR') ?: '';
        $userAnalyzator = $this->getUserFromDbByLogin($userAnalyzatorLogin);
        if ($userAnalyzator->id() === 0) {
            throw new \RuntimeException("System analyzator user not found");
        }

        return $userAnalyzator;
    }

    /**
     * @param ?array $userData
     * @return User
     */
    private function getUser(?array $userData): User
    {
        $user = new User();
        if ($userData === null) {
            return $user;
        }
        $user->setData($userData);

        $userGroupsData = $this->mUserGroups->getUserGroups($user->id());

        $userGroups = [];
        foreach ($userGroupsData as $userGroupData) {
            $userGroups[] = new UserGroup($userGroupData);
        }

        $user->setGroups($userGroups);

        return $user;
    }

    /**
     * Сохраняет зарегистрированного пользователя в базу
     * @param string $login
     * @param string $password
     * @return int
     */
    public function saveRegisteredUserToDb(string $login, string $password): int
    {
        try {
            $userId = $this->mUsers->insert([
                'login' => $login,
                'password' => $this->mUsers->hash($password),
                'is_blocked' => 1,
                'blocking_reason' => self::BLOCK_REASON_REGISTRATION_NOT_FINISHED,
                'blocked_at' => date('Y-m-d H:i:s'),
            ]);
        } catch (Exception) {
            $userId = 0;
        }

        return $userId;
    }

    /**
     * Блокирует пользователя
     * @param int $userId
     * @param string $reason
     * @return bool
     */
    public function blockUser(int $userId, string $reason): bool
    {
        try {
            // TODO: обрабатывать кейс когда пользователь уже заблокирован
            $isBlocked = (bool) $this->mUsers->update([
                'is_blocked' => 1,
                'block_reason' => $reason,
                'blocked_at' => date('Y-m-d H:i:s'),
            ], ['user_id' => $userId]);
        } catch (Exception) {
            $isBlocked = false;
        }

        return $isBlocked;
    }

    /**
     * Разблокирует пользователя
     * @param int $userId
     * @return bool
     */
    public function unblockUser(int $userId): bool
    {
        try {
            $isUnblocked = (bool) $this->mUsers->update([
                'is_blocked' => 0,
                'blocking_reason' => '',
                'blocked_at' => null,
            ], ['user_id' => $userId]);
        } catch (Exception) {
            $isUnblocked = false;
        }
        return $isUnblocked;
    }

    /**
     * @param int $userId
     * @param string $password
     * @return bool
     */
    public function setPassword(int $userId, string $password): bool
    {
        try {
            $saved = (bool) $this->mUsers->update(
                ['password' => $this->mUsers->hash($password)],
                ['user_id' => $userId]
            );
        } catch (Exception) {
            $saved = false;
        }
        return $saved;
    }

    // endregion DB Interaction
}