<?php
declare(strict_types=1);
namespace Zf3Lib\User\Service;

use Zf3Lib\User\Service\Validator\Password;

class RestorePasswordService
{
    protected UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    // region Data, Setters & Getters

    protected array $data = [];

    /**
     * Возвращает пароль
     */
    public function password(): string
    {
        return $this->data['password'] ?? '';
    }

    /**
     * Устанавливает пароль
     */
    public function setPassword(string $password): static
    {
        $this->data['password'] = $password;
        return $this;
    }

    /**
     * Возвращает значение надёжности пароля
     * @return int
     */
    public function passwordStrength(): int
    {
        return (int) ($this->data['password_strength'] ?? 0);
    }

    /**
     * Устанавливает значение надёжности пароля
     */
    public function setPasswordStrength(int $passwordStrength): static
    {
        $this->data['password_strength'] = $passwordStrength;
        return $this;
    }

    /**
     * Устанавливает данные
     */
    public function setData(array $data): static
    {
        $this->data = $data;
        return $this;
    }

    // endregion Data, Setters & Getters


    // region Filter

    /**
     * Фильтрует полученные данные
     */
    public function filter(): static
    {
        $this->filterPassword();

        return $this;
    }

    /**
     * Фильтрует пароль
     */
    protected function filterPassword(): void
    {
        $this->setPassword(trim($this->password()));
    }

    // endregion Filter


    // region Validate

    /**
     * Валидирует полученные данные
     */
    public function validate(): static
    {
        $this->errors = [];
        $this->validatePassword();
        return $this;
    }

    /**
     * Валидирует пароль
     */
    protected function validatePassword(): void
    {
        $passwordValidator = new Password();
        if (!$passwordValidator->isValid($this->password())) {
            $this->errors['password'] = $passwordValidator->getMessages();
        }
        $this->setPasswordStrength($passwordValidator->strengthLevel());
    }

    // endregion Validate


    // region Profit

    /**
     * Валидна ли форма регистрации
     */
    public function isValid(): bool
    {
        return (count($this->errors) === 0);
    }

    /**
     * Валиден ли пароль
     */
    public function isValidPassword(): bool
    {
        return empty($this->errors['password']);
    }


    protected array $errors = [];

    /**
     * Возвращает массив ошибок
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    // endregion Profit


    // region Restore Password

    public const RESTORE_PASSWORD_ERROR_EMPTY_TOKEN            = '__auth_set_password_error__empty_token__';
    public const RESTORE_PASSWORD_ERROR_WRONG_TOKEN            = '__auth_set_password_error__wrong_token__';
    public const RESTORE_PASSWORD_ERROR_USER_DOESNT_EXIST      = '__auth_set_password_error__user_doesnt_exist__';
    public const RESTORE_PASSWORD_ERROR_COULDNT_SET_PASSWORD   = '__auth_set_password_error__couldnt_set_password__';
    public const RESTORE_PASSWORD_MESSAGE_PASSWORD_IS_SET      = '__auth_set_password_message__password_is_set__';

    public function savePassword(string $login, string $password): bool
    {
        $this->errors = [];
        $user = $this->userService->getUserFromDbByLogin($login);

        if ($user->id() === 0) {
            $this->errors['set_password'] = [self::RESTORE_PASSWORD_ERROR_USER_DOESNT_EXIST];
            return false;
        }

        if (!$this->userService->setPassword($user->id(), $password)) {
            $this->errors['set_password'] = [self::RESTORE_PASSWORD_ERROR_COULDNT_SET_PASSWORD];
            return false;
        }

        return true;
    }

    // endregion Restore Password
}