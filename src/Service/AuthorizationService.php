<?php
declare(strict_types=1);
namespace Zf3Lib\User\Service;

use DateTimeImmutable;
use Laminas\Http\PhpEnvironment\Request;
use Zf3Lib\User\DbGateway\User\Users;
use Zf3Lib\User\Entity\AccessToken;
use Zf3Lib\User\Entity\Authorization;
use Laminas\Session\Container as SessionContainer;

class AuthorizationService
{
    private Users $mUsers;
    private AccessTokenService $accessTokenService;
    private Request $request;

    public function __construct(Users $mUsers, AccessTokenService $accessTokenService, Request $request)
    {
        $this->mUsers = $mUsers;
        $this->accessTokenService = $accessTokenService;
        $this->request = $request;
    }

    /**
     * Выполняет авторизацию:
     * - либо указанным методом
     * - либо всеми доступными метода, если метод не задан через параметр
     * @param ?string $method
     * @return Authorization
     */
    public function get(?string $method = null): Authorization
    {
        $auth = new Authorization();

        if ($method === Authorization::AUTH_METHOD__BY_SESSION || $method === null) {
            $auth = $this->bySession();
            if ($method !== null) return $auth;
        }
        if ($auth->isAuthorized()) return $auth;

        if ($method === Authorization::AUTH_METHOD__BY_QUERY || $method === null) {
            $auth = $this->byQuery();
            if ($method !== null) return $auth;
        }
        if ($auth->isAuthorized()) return $auth;

        if ($method === Authorization::AUTH_METHOD__BY_TOKEN || $method === null) {
            $auth = $this->byToken();
            if ($method !== null) return $auth;
        }
        if ($auth->isAuthorized()) return $auth;

        return new Authorization();
    }


    // region Authorization Methods

    /**
     * Авторизует по сессии
     * @return Authorization
     */
    private function bySession(): Authorization
    {
        $sessionContainer = new SessionContainer('auth');
        $login = $sessionContainer->offsetGet('login') ?? '';

        $auth = (new Authorization())
            ->setAuthMethod(Authorization::AUTH_METHOD__BY_SESSION)
            ->setLogin($login);

        if ($login === '') {
            return $auth
                ->setIsAuthorized(false)
                ->setAuthorizationFailReason(Authorization::AUTH_FAIL_REASON__USER_NOT_LOGGED);
        }

        $userData = $this->getUserDataByLogin($login);
        $this->processUserData($userData, $auth);

        return $auth;
    }

    /**
     * Авторизует по get- или post-запросу
     * @return Authorization
     */
    private function byQuery(): Authorization
    {
        $login    = $this->request->getPost('login') ?? $this->request->getQuery('login') ?? '';
        $password = $this->request->getPost('password') ?? $this->request->getQuery('password') ?? '';

        $auth = (new Authorization())
            ->setAuthMethod(Authorization::AUTH_METHOD__BY_QUERY)
            ->setLogin($login);

        if ($login === '' || $password === '') {
            return $auth
                ->setIsAuthorized(false)
                ->setAuthorizationFailReason(
                    ($login === '')
                        ? Authorization::AUTH_FAIL_REASON__EMPTY_LOGIN
                        : Authorization::AUTH_FAIL_REASON__EMPTY_PASSWORD
                );
        }

        $userData = $this->getUserDataByLogin($login);
        $this->processUserData($userData, $auth);

        if ($auth->isAuthorized()) {
            $hashStored = $userData['password'] ?? '';
            $hashPassed = $this->getPasswordHash($password);
            if ($hashPassed !== $hashStored) {
                $auth->setIsAuthorized(false)
                    ->setAuthorizationFailReason(Authorization::AUTH_FAIL_REASON__WRONG_PASSWORD);
            }
        }

        return $auth;
    }

    /**
     * Авторизует по токену
     * @return Authorization
     */
    private function byToken(): Authorization
    {
        $auth = (new Authorization())->setAuthMethod(Authorization::AUTH_METHOD__BY_TOKEN);

        $tokenHash = $this->request->getPost('token') ?? $this->request->getQuery('token');
        if ($tokenHash === null) {
            return $auth->setIsAuthorized(false)
                ->setAuthorizationFailReason(Authorization::AUTH_FAIL_REASON__EMPTY_TOKEN);
        }

        $token = $this->getAccessTokenByHash($tokenHash);
        if ($token === null) {
            return $auth->setIsAuthorized(false)
                ->setAuthorizationFailReason(Authorization::AUTH_FAIL_REASON__TOKEN_NOT_FOUND);
        }

        $auth->setAccessToken($token);
        if ($token->validUntil() !== null
            && $token->validUntil()->getTimestamp() < (new DateTimeImmutable('now'))->getTimestamp()
        ) {
            $auth->setIsAuthorized(false)
                ->setAuthorizationFailReason(Authorization::AUTH_FAIL_REASON__TOKEN_EXPIRED);
        }

        $userData = $this->getUserDataById($token->userId());
        $this->processUserData($userData, $auth);

        return $auth;
    }

    // endregion Authorization Methods


    // region DB Interaction

    private function getUserDataByLogin(string $login): ?array
    {
        return $this->mUsers->getByLogin($login);
    }

    private function getUserDataById(int $userId): ?array
    {
        return $this->mUsers->getById($userId);
    }

    private function getPasswordHash(string $password): string
    {
        return $this->mUsers->hash($password);
    }

    private function getAccessTokenByHash(string $hash): ?AccessToken
    {
        return $this->accessTokenService->getByHash($hash);
    }

    // endregion DB Interaction


    private function processUserData(?array $userData, Authorization $auth): void
    {
        if ($userData === null) {
            $auth->setIsAuthorized(false)
                ->setAuthorizationFailReason(Authorization::AUTH_FAIL_REASON__USER_NOT_FOUND);
            return;
        }

        if ($userData['is_blocked']) {
            $auth->setIsAuthorized(false)
                ->setAuthorizationFailReason(Authorization::AUTH_FAIL_REASON__USER_BLOCKED);
            return;
        }

        $auth->setIsAuthorized(true);
    }
}