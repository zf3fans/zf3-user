<?php
declare(strict_types=1);
namespace Zf3Lib\User\Service;

use DateTimeImmutable;
use JetBrains\PhpStorm\Pure;
use Zf3Lib\User\Entity\AccessToken;
use Zf3Lib\User\DbGateway\User\UserAccessTokens;
use Zf3Lib\Lib\Helper\Arr;
use Laminas\Db\Sql;

class AccessTokenService
{
    const API_TOKENS_LIMIT = 3;

    private UserAccessTokens $mUserAccessTokens;

    public function __construct(UserAccessTokens $userAccessTokens)
    {
        $this->mUserAccessTokens = $userAccessTokens;
    }

    /**
     * Возвращает уникальный хеш
     * @return string
     */
    public function uniqueHash(): string
    {
        return md5(uniqid("access_token", true) . date('Y-m-d H:i:s'));
    }

    #[Pure]
    public function salt(): string
    {
         return $this->uniqueHash();
    }

    /**
     * @param int $userId
     * @param string $tokenType
     * @param DateTimeImmutable|null $validUntil
     * @return AccessToken
     */
    public function generate(int $userId, string $tokenType, ?DateTimeImmutable $validUntil = null): AccessToken
    {
        if ($validUntil === null) {
            $validUntil = $this->getValidUntilFromNow();
        }

        $tokenHash = $this->uniqueHash();
        $this->mUserAccessTokens->insert([
            'token_hash' => $tokenHash,
            'token_salt' => $this->salt(),
            'token_type' => in_array($tokenType, AccessToken::TYPES) ? $tokenType : '',
            'user_id' => $userId,
            'valid_until' => $validUntil->format('Y-m-d H:i:s'),
        ]);

        return $this->getByHash($tokenHash);
    }

    /**
     * Возвращает дату валидности токена
     * @return DateTimeImmutable
     */
    public function getValidUntilFromNow(): DateTimeImmutable
    {
        return (new DateTimeImmutable())->modify('+50 year');
    }

    /**
     * @param string $hash
     * @return ?AccessToken
     */
    public function getByHash(string $hash): ?AccessToken
    {
        $tokenData = $this->mUserAccessTokens->findOne(['token_hash' => $hash]);
        return ($tokenData !== null)
            ? new AccessToken($tokenData)
            : null;
    }

    /**
     * Инвалидирует токен
     * @param AccessToken $accessToken
     */
    public function invalidate(AccessToken $accessToken): void
    {
        $this->invalidateByHash($accessToken->hash(), $accessToken->type());
    }

    /**
     * Инвалидирует токен по хешу
     * @param string $hash
     * @param ?string $type
     */
    public function invalidateByHash(string $hash, ?string $type): void
    {
        $validUntil = (new DateTimeImmutable())->format('Y-m-d H:i:s');

        $where = [
            'token_hash' => $hash,
        ];
        if ($type !== null) {
            $where['token_type'] = $type;
        }


        $this->mUserAccessTokens->update([
            'valid_until' => $validUntil
        ], $where);
    }

    /**
     * Возвращает условия поиска по параметрам
     * @param array $params
     * @return Sql\Where
     */
    private function _getWhereByParams(array $params): Sql\Where
    {
        $where = new Sql\Where();

        foreach (['token_id', 'token_hash', 'token_type', 'user_id'] as $field) {
            if (isset($params[$field])) {
                if (is_array($params[$field])) {
                    $values = Arr::filterArrayOfString($params[$field]);
                    if (count($values) > 0) {
                        $where->addPredicate(
                            new Sql\Predicate\In(
                                $field,
                                $values
                            )
                        );
                    }
                } else {
                    $where->addPredicate(
                        new Sql\Predicate\Operator(
                            $field,
                            Sql\Predicate\Operator::OP_EQ,
                            $params[$field]
                        )
                    );
                }
            }
        }

        if (isset($params['is_active'])) {
            $where->addPredicate(
                new Sql\Predicate\Operator(
                    'valid_until',
                    ($params['is_active'])
                        ? Sql\Predicate\Operator::OP_GT
                        : Sql\Predicate\Operator::OP_LT,
                    date('Y-m-d H:i:s')
                ),
            );
        }

        return $where;
    }

    /**
     * Возвращает лимит поиска по параметрам
     * @param array $params
     * @return ?int
     */
    private function _getLimitByParams(array $params): ?int
    {
        if (!isset($params['limit'])) {
            return null;
        }

        return (int) $params['limit'];
    }

    /**
     * Возвращает массив токенов по параметрам
     * @param array $params
     * @return array
     */
    public function getList(array $params): array
    {
        $accessTokens = [];
        $accessTokensData = $this->mUserAccessTokens->findList(
            $this->_getWhereByParams($params),
            null,
            $this->_getLimitByParams($params)
        );

        foreach ($accessTokensData as $accessTokenData) {
            $accessTokens[] = new AccessToken($accessTokenData);
        }

        return $accessTokens;
    }

    public function getCurrentActive(int $userId, string $tokenType = AccessToken::TYPE_API): ?AccessToken
    {
        return Arr::first($this->getList([
            'user_id' => $userId,
            'token_type' => $tokenType,
            'is_active' => true,
        ]));
    }

    public function getOrGenerate(int $userId, string $tokenType = AccessToken::TYPE_API, ?int $limit = null): ?AccessToken
    {
        $accessToken = $this->getCurrentActive($userId, $tokenType);

        if ($accessToken === null) {
            $accessTokensInactive = $this->getList([
                'user_id' => $userId,
                'token_type' => AccessToken::TYPE_API,
                'is_active' => false,
            ]);

            if (
                ($limit === null)
                || (count($accessTokensInactive) < $limit)
            ) {
                $accessToken = $this->generate(
                    $userId,
                    $tokenType
                );
            }
        }
        return $accessToken;
    }
}
