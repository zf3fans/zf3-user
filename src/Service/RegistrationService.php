<?php
declare(strict_types=1);
namespace Zf3Lib\User\Service;

use Laminas\Validator\EmailAddress;

class RegistrationService extends RestorePasswordService
{
    // region Data, Setters & Getters

    /**
     * Возвращает логин
     * @return string
     */
    public function login(): string
    {
        return $this->data['login'] ?? '';
    }

    /**
     * Устанавливает логин
     */
    public function setLogin(string $login): static
    {
        $this->data['login'] = $login;
        return $this;
    }

    // endregion Data, Setters & Getters


    // region Filter

    /**
     * Фильтрует полученные данные
     */
    public function filter(): static
    {
        $this->filterEmail();
        $this->filterPassword();

        return $this;
    }

    /**
     * Фильтрует email
     */
    protected function filterEmail(): void
    {
        $this->setLogin(trim($this->login()));
    }

    // endregion Filter


    // region Validate

    /**
     * Валидирует полученные данные
     */
    public function validate(): static
    {
        $this->errors = [];

        $this->validateEmail();
        $this->validatePassword();

        return $this;
    }

    /**
     * Валидирует email
     */
    protected function validateEmail(): void
    {
        $emailValidator = new EmailAddress();
        if (!$emailValidator->isValid($this->login())) {
            $this->errors['login'] = $emailValidator->getMessages();
        }
    }

    // endregion Validate


    // region Profit

    /**
     * Валиден ли логин
     * @return bool
     */
    public function isValidLogin(): bool
    {
        return empty($this->errors['login']);
    }

    // endregion Profit


    // region Register

    public const ERROR_LOGIN_ALREADY_EXIST  = 'register__login_already_exist';
    public const ERROR_DB_EXCEPTION         = 'register__db_exception';
    public const MESSAGE_SUCCESS            = 'register__success';
    public const MESSAGE_FINISH_SUCCESS     = 'register__finish-success';
    public const MESSAGE_FINISH_FAIL        = 'register__finish-fail';

    /**
     * Регистрирует пользователя
     * В случае успешной регистрации возвращает id пользователя. В противном случае возвращает 0.
     *
     * @param string $login
     * @param string $password
     * @return int
     */
    public function register(string $login, string $password): int
    {
        $this->errors = [];

        /** @var UserService $userService */
        $userService = $this->serviceManager->get(UserService::class);
        $user = $userService->getUserFromDbByLogin($login);

        if ($user->id() !== 0) {
            $this->errors['register'] = [self::ERROR_LOGIN_ALREADY_EXIST];
            return 0;
        }

        $userId = $userService->saveRegisteredUserToDb($login, $password);
        if ($userId === 0) {
            $this->errors['register'] = [self::ERROR_DB_EXCEPTION];
            return 0;
        }

        return $userId;
    }

    // endregion Register
}
