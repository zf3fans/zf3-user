<?php
declare(strict_types=1);
namespace Zf3Lib\User\Service\Validator;

use Laminas\Validator\AbstractValidator;

class Password extends AbstractValidator
{
    public const ERROR_NOT_A_STRING           = 'passwordNotAString';
    public const ERROR_IS_EMPTY               = 'passwordEmptyValue';
    public const ERROR_UNSUPPORTED_CHARACTERS = 'passwordUnsupportedCharacters';

    protected array $messageTemplates = [
        self::ERROR_NOT_A_STRING           => "Invalid type given. String expected",
        self::ERROR_IS_EMPTY               => "The input is empty",
        self::ERROR_UNSUPPORTED_CHARACTERS => "The input contains unsupported characters: %unsupportedCharacters%",
    ];

    protected array $messageVariables = [
        'unsupportedCharacters'  => 'unsupportedCharacters',
    ];

    protected string $unsupportedCharacters = '';

    public const STRENGTH_NONE        = 0;
    public const STRENGTH_EASY        = 1;
    public const STRENGTH_NORMAL      = 2;
    public const STRENGTH_STRONG      = 3;
    public const STRENGTH_VERY_STRONG = 4;

    private int $strengthLevel = self::STRENGTH_NONE;

    /**
     * Устанавливает значение надёжности пароля
     * @param int $level
     */
    private function setStrengthLevel(int $level): void
    {
        $this->strengthLevel = (
               $level >= self::STRENGTH_NONE
            && $level <= self::STRENGTH_VERY_STRONG
        )
            ? $level
            : self::STRENGTH_NONE;
    }

    /**
     * Определяет значение надёжности пароля
     */
    private function detectStrength(): int
    {
        $length = mb_strlen($this->getValue());

        $countDigits    = (int) preg_match_all('#[0-9]#', $this->getValue(), $matchDigits);
        $countCharsLow  = (int) preg_match_all('#[a-z]#', $this->getValue(), $matchCharsLow);
        $countCharsHigh = (int) preg_match_all('#[A-Z]#', $this->getValue(), $matchCharsHigh);
        $countCharsSpec = (int) preg_match_all('#[`~!@\#\$%^&*()\-_+=\'":;,./\\\><\[\]]#', $this->getValue(), $matchCharsSpec);

        $count = $countDigits + $countCharsLow + $countCharsHigh + $countCharsSpec;


        $this->strengthLevel = match (true) {
            ($count <= 2 || $length <= 6)  => self::STRENGTH_EASY,
            ($count <= 3 || $length <= 7)  => self::STRENGTH_NORMAL,
            ($count <= 4 || $length <= 8)  => self::STRENGTH_STRONG,
            ($count <= 4 || $length <= 10) => self::STRENGTH_VERY_STRONG,
        };

        return $this->strengthLevel;
    }

    /**
     * Надёжность пароля
     * @return int
     */
    public function strengthLevel(): int
    {
        return $this->strengthLevel;
    }

    /**
     * Валидный ли пароль
     * @param mixed $value
     * @return bool
     */
    public function isValid($value): bool
    {
        $this->setStrengthLevel(self::STRENGTH_NONE);
        if (!is_string($value)) {
            $this->error(self::ERROR_NOT_A_STRING);
            return false;
        }

        $this->setValue($value);

        if ($this->getValue() === '') {
            $this->error(self::ERROR_IS_EMPTY);
            return false;
        }


        if (preg_match_all('#[^a-zA-Z0-9`~!@\#\$%^&*()\-_+=\'":;,./\\\><\[\]]#', $this->getValue(), $match)) {
            $this->unsupportedCharacters = implode('', array_unique($match[0]));
            $this->error(self::ERROR_UNSUPPORTED_CHARACTERS);
            return false;
        }

        $this->detectStrength();

        return true;
    }
}
