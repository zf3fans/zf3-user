<?php
declare(strict_types=1);
namespace Zf3Lib\User;

return [
    'router' => [
        'routes' => [
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            'user' => __DIR__ . '/../view',
        ],
    ],
];
