-- Users

drop table if exists users;
create table users
(
    `user_id`         int unsigned auto_increment
        primary key,
    `login`           varchar(255) default '' not null,
    `password`        varchar(32)  default '' not null,
    `is_blocked`      tinyint(1)   default 0  not null,
    `blocking_reason` varchar(255) default '' not null,
    `blocked_at`      datetime                null,
    `created_at`      datetime                not null,
    `updated_at`      datetime                not null
);

drop table if exists user_groups;
create table user_groups
(
    `group_id`   int unsigned auto_increment
        primary key,
    `slug`       varchar(100) not null,
    `name`       varchar(100) not null,
    `created_at` datetime     null,
    `updated_at` datetime     null,
    constraint user_groups_slug_IDX
        unique (`slug`)
);
insert into user_groups (`group_id`, `slug`, `name`, `created_at`, `updated_at`)
values
    (1, 'admin', 'Administrator', NOW(), NOW());

drop table if exists user_group_links;
create table user_group_links
(
    `user_id`   int unsigned not null,
    `group_id`  int unsigned not null,
    `linked_at` datetime     not null,
    constraint user_group_links_user_id_IDX
        unique (`user_id`, `group_id`)
);

drop table if exists user_access_tokens;
create table user_access_tokens
(
    `token_id`    bigint unsigned auto_increment
        primary key,
    `token_hash`  char(32)                                 not null,
    `token_salt`  char(32)                                 null,
    `token_type`  enum ('api', 'auth', 'password_recover') not null,
    `user_id`     int unsigned                             not null,
    `created_at`  datetime                                 not null,
    `valid_until` datetime                                 not null
);

drop table if exists user_api_requests;
create table user_api_requests
(
    `request_id`            bigint unsigned auto_increment
        primary key,
    `request_action`        varchar(255)                            default ''        not null,
    `request_result`        enum ('accept', 'reject')                                 not null,
    `request_result_reason` varchar(255)                            default ''        not null,
    `token_id`              bigint unsigned                         default 0         not null,
    `token_hash`            varchar(255)                            default ''        not null comment 'Received token hash value',
    `user_id`               int unsigned                            default 0         not null,
    `solution_id`           int unsigned                            default 0         not null,
    `solution_platform`     enum ('mt4', 'mt5', 'unknown')        default 'unknown' not null,
    `solution_category`     enum ('expert', 'indicator', 'unknown') default 'unknown' not null,
    `solution_version`      varchar(30)                             default ''        not null,
    `ip`                    varchar(39)                                               not null,
    `requested_at`          datetime                                                  not null
);